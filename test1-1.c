/*
 * test1-1.c
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */

#include "arduino.h"
#include "res_hw.h"

void test11_setup(void)
{
	int lc;
	for(lc = 0; lc < 8; lc++ ) {
		pinMode(pin_led[lc], OUTPUT);
		digitalWrite(pin_led[lc], LOW);
	}
}

void test11_loop(void) {
}
