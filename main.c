/*
 * 組み込みソフトウェア　ライブラリの単体テストコード
 * main.c
 */
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "utils/uartstdio.h"
#include "utils/ustdlib.h"


/* デバッグマクロ */
#define __DEBUG__

// Setup debug printing macros.
#ifdef __DEBUG__
  #define DPRINTF(...) { UARTprintf(__VA_ARGS__); }
#else // Release
  #define DPRINTF(...) {}
#endif // __DEBUG__


//#define BAUDRATE 115200	/* シリアル通信のボーレート */
#define BAUDRATE 9600		/* シリアル通信のボーレート */
#define PORT_NUM 0			/* UARTポート番号 */

#define MAX_CMD_LEN 255		// 受信バッファのサイズ
char g_cmdbuff[MAX_CMD_LEN];// コマンドを受信するバッファ


#include "arduino.h"	// Arduino API
#include "test1-1.h"	// テスト項目番号1-1

#include "test2-1.h"	// テスト項目番号2-1

#include "test3-1.h"	// テスト項目番号3-1

#include "test4-1.h"	// テスト項目番号4-1
#include "test4-2.h"	// テスト項目番号4-2
#include "test4-3.h"	// テスト項目番号4-3


#include "driverlib/sysctl.h"
void main(void)
{
	// 動作クロックの設定
	SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    char testmode = 255;// テストコードの番号

	// UARTポートの初期化処理
	SerialBegin(PORT_NUM, BAUDRATE);
    UARTprintf("start\n");
    UARTprintf(" Baudrate: %d[bps]", BAUDRATE);
    UARTprintf(" Enter \\q to exit.\n");
    UARTprintf(" Enter test [number] to run test code.\n");

    char *cmd = g_cmdbuff;

    const int LOOP_PERIOD = 10;
    uint32_t counter = 0;
    for(;;counter++){
    	delay(LOOP_PERIOD/2);
    	delayMicroseconds(LOOP_PERIOD/2*1000);
        // 1秒毎に . を出力
    	if( 0 == counter%(1000/LOOP_PERIOD) ){
    		SerialPut(PORT_NUM,'.');
    	}

        if(SerialAvailable(0)){
			cmd = g_cmdbuff;

			*cmd = SerialRead(PORT_NUM);
            while( '\0' != *cmd ) {
            	*(++cmd) = SerialRead(PORT_NUM);
            }
            cmd = g_cmdbuff;

            while (*cmd == ' ')
                cmd += 1;
            if (ustrncmp(cmd, "\\q", 2) == 0)
                break;

            if (ustrncmp(cmd, "echo", 4) == 0) {
            	// 二番目の文字列をエコーバックする
                cmd = ustrstr(cmd, " ");
                if (cmd != 0)
                    UARTprintf("%s\n", cmd+1);
            }

            if (ustrncmp(cmd, "test", 4) == 0) {
            	// テストコードを選択する
                cmd = ustrstr(cmd, " ");
                if (cmd != 0) {
                    UARTprintf("run test%s\n", cmd+1);
                    testmode = atoi(cmd+1);
                    break;
                }
            }

        } // if(SerialAvailable ...
    }	// for ...

    DPRINTF("Initializing...\n");

    // コマンドに従って、テスコード（初期化処理）を選択するト
    switch(testmode) {
    case 11 :
    	test11_setup();
    	break;
    case 21 :
    	test21_setup();
    	break;
    case 31 :
    	test31_setup();
    	break;
    case 41 :
    	test41_setup();
    	break;
    case 42 :
    	test42_setup();
    	break;
    case 43 :
    	test43_setup();
    	break;
    default:
    	break;
    }

    DPRINTF("Into main loop...\n");

    // メインループ
	for(;;) {
        // コマンドに従って、テスコード（ループ処理）を選択するト
		switch(testmode) {
		case 11 :
	    	test11_loop();
			break;
		case 21 :
	    	test21_loop();
			break;
		case 31 :
	    	test31_loop();
			break;
		case 41 :
	    	test41_loop();
			break;
		case 42 :
	    	test42_loop();
			break;
	    case 43 :
	    	test43_loop();
	    	break;
	    default:
	    	break;
		}
	}

}	// end of main


//////////////////////////////////////////////
// 4月15日のサンプルコード
//#include "inc/hw_types.h"
//#include "inc/hw_memmap.h"
//#include "driverlib/gpio.h"
//#include "driverlib/adc.h"
//#include "driverlib/pin_map.h"
//#include "driverlib/pwm.h"
//
//#include "pin_assign.h"
//#define MAX_ADC_COUNT 4096
//
//#define BAUDRATE 9600	/* シリアル通信のボーレート */
//
//#define PORT_NUM 0	/* UARTポート番号 */
//// 受信バッファのサイズ
//#define MAX_CMD_LEN 255
//// コマンドを受信するバッファ
//char g_cmdbuff[MAX_CMD_LEN];
//
//
//void main(void)
//{
//	PinName pin_volume	= PIN_PE3;
//	PinName pin_led 	= PIN_PD0;
//
//	uint32_t peripheral_volume = 0;
//	uint32_t peripheral_led = 0;
//	uint32_t pwm_gpio_pin_num = 0;
//
//	uint32_t peripheral_volume_adc = 0;
//	uint32_t adc_base_address = ADC0_BASE;
//	uint32_t adc_gpio_pin_num = 0;
//
//	uint32_t peripheral_led_pwm = 0;
//	uint32_t pwm_base_address = 0;
//	uint32_t pwm_module_address = 0;
//	uint32_t pwm_pin = 0;
//	uint32_t pwm_pin_param = 0;
//	uint32_t pwm_gen = 0;
//	uint32_t pwm_out = 0;
//	uint32_t pwm_out_bit = 0;
//
//
//
//	// ピン番号からペリフェラルのアドレスなどを決める
//	if( PORT_E == g_pintable[pin_volume].peripheral_port) {
//		peripheral_volume = SYSCTL_PERIPH_GPIOE;
//	}
//
//	// ピン番号からペリフェラルのアドレスなどを決める
//	if( PORT_D == g_pintable[pin_led].peripheral_port) {
//		peripheral_led = SYSCTL_PERIPH_GPIOD;
//		pwm_base_address = GPIO_PORTD_BASE;
//	}
//	if( 0 == g_pintable[pin_led].peripheral_pin) {
//		pwm_gpio_pin_num = GPIO_PIN_0;
//	}
//
//	peripheral_volume_adc = SYSCTL_PERIPH_ADC0;
//	if( 3 == g_pintable[pin_volume].peripheral_pin ) {
//		adc_gpio_pin_num = GPIO_PIN_3;
//	}
//
//	// PWMピン番号からPWMパラメータを決める
//	if( NA != g_pintable[pin_led].pwm[0].pin ) {
//		peripheral_led_pwm = SYSCTL_PERIPH_PWM0;
//		pwm_module_address = PWM0_BASE;
//		pwm_pin = g_pintable[pin_led].pwm[0].pin;
//		pwm_pin_param = g_pintable[pin_led].pwm[0].param;
//	}
//	else if( NA != g_pintable[pin_led].pwm[1].pin ) {
//		peripheral_led_pwm = SYSCTL_PERIPH_PWM1;
//		pwm_pin = g_pintable[pin_led].pwm[1].pin;
//		pwm_pin_param = g_pintable[pin_led].pwm[1].param;
//	}
//
//	// PWMジェネレータを決める
//	switch(pwm_pin) {
//	case 0 :	case 1 :
//		pwm_gen = PWM_GEN_0;
//		break;
//	case 2 :	case 3 :
//		pwm_gen = PWM_GEN_1;
//		break;
//	case 4 :	case 5 :
//		pwm_gen = PWM_GEN_2;
//		break;
//	case 6 :	case 7 :
//		pwm_gen = PWM_GEN_3;
//		break;
//	default:
//		break;
//	}
//
//	// PWMのパラメータをPWMピン番号から決める
//	switch(pwm_pin) {
//	case 0 :
//		pwm_out = PWM_OUT_0;
//		pwm_out_bit = PWM_OUT_0_BIT;
//		break;
//	case 1 :
//		pwm_out = PWM_OUT_1;
//		pwm_out_bit = PWM_OUT_1_BIT;
//		break;
//	case 2 :
//		pwm_out = PWM_OUT_2;
//		pwm_out_bit = PWM_OUT_2_BIT;
//		break;
//	case 3 :
//		pwm_out = PWM_OUT_3;
//		pwm_out_bit = PWM_OUT_3_BIT;
//		break;
//	case 4 :
//		pwm_out = PWM_OUT_4;
//		pwm_out_bit = PWM_OUT_4_BIT;
//		break;
//	case 5 :
//		pwm_out = PWM_OUT_5;
//		pwm_out_bit = PWM_OUT_5_BIT;
//		break;
//	case 6 :
//		pwm_out = PWM_OUT_6;
//		pwm_out_bit = PWM_OUT_6_BIT;
//		break;
//	case 7 :
//		pwm_out = PWM_OUT_7;
//		pwm_out_bit = PWM_OUT_7_BIT;
//		break;
//	default:
//		break;
//	}
//
//
//	// 使用するペリフェラルの有効化　※LED用にIOポートDを使用
//	SysCtlPeripheralEnable(peripheral_led);
//	SysCtlPeripheralEnable(peripheral_volume);
//
//	// A/D変換モジュール0を有効化
//	SysCtlPeripheralEnable(peripheral_volume_adc);
//
//	// PWMモジュール0を有効化
//	SysCtlPeripheralEnable(peripheral_led_pwm);
//
//	// ADCの設定
//	// PE3(AIN0)をADCに設定
//	GPIOPinTypeADC(GPIO_PORTE_BASE, adc_gpio_pin_num);
//
//	////////////////////////////
//	// 使用するADCシーケンサの指定
//	ADCSequenceConfigure(adc_base_address, 3, ADC_TRIGGER_PROCESSOR, 0);
//	// ADCシーケンサの設定
//	ADCSequenceStepConfigure(adc_base_address, 3, 0, ADC_CTL_CH0 | ADC_CTL_IE | ADC_CTL_END);
//	////////////////////////////
//
//	// ADCシーケンサを有効にする
//	ADCSequenceEnable(adc_base_address, 3);
//
//	// PWMの設定
//	GPIOPinTypePWM(pwm_base_address, pwm_gpio_pin_num);
//	//
//	GPIOPinConfigure(pwm_pin_param);
//	// PWMの周期設定
//	unsigned long ulPeriod = SysCtlClockGet() / 1000;
//	PWMGenConfigure(pwm_module_address, pwm_gen, PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);
//	// PWMジェネレーターに制御周期を設定する
//	PWMGenPeriodSet(pwm_module_address, pwm_gen, ulPeriod);
//	// PWM信号のパルス幅（デューティー比より）を設定する
//	PWMPulseWidthSet(pwm_module_address, pwm_out, ulPeriod * 9 / 10);
//	// PWM0の出力を許可する
//	PWMOutputState(pwm_module_address, pwm_out_bit, true);
//	// PWM制御を開始する
//	PWMGenEnable(pwm_module_address, pwm_gen);
//
//	// A/Dの値
//	uint32_t ulData = 0;
//
//	// 無限ループ
//	for(;;) {
//		// ADC 変換処理を開始
//		ADCProcessorTrigger(adc_base_address, 3);
//		// ADCの値を読み取る
//		ADCSequenceDataGet(adc_base_address, 3, &ulData);
//
//		// A/D変換の値からディーティー比を変更する
//		PWMPulseWidthSet(pwm_module_address, pwm_out, ulPeriod * (MAX_ADC_COUNT - ulData) / (MAX_ADC_COUNT+1));
//	}
//}
