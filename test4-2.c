/*
 * test4-2.c
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */
#include "arduino.h"
#include "res_hw.h"

void test42_setup(void)
{
	pinMode(pin_motor[0], PWM);
	pinMode(pin_motor[1], PWM);

	pinMode(pin_vr[0], ADC);
}

void test42_loop(void)
{
	// アナログ入力の値を読み取る
	unsigned long ulData = analogRead(pin_vr[0]);

	// A/D変換の値からアナログ出力を変更する
	analogWrite(pin_motor[1], ulData);
}
