/*
 * arduino.h
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */

/*======================================================================================*
 *																						*
 * 「ARMチャレンジャー」Arduino互換ライブラリ化　ヘッダーファイル								*
 * Revision: 1.0																		*
 * Author: SSL N.N.																		*
 * Date: 2016/02/02																		*
 *																						*
 *======================================================================================*/
#ifndef ARDUINO_H_
#define ARDUINO_H_


#include "pin_assign.h"


typedef enum tagDigitalValue { LOW=0, HIGH } DigitalValue;
typedef enum tagPinMode { INPUT, OUTPUT, INPUT_PULLUP, ADC, PWM } PinMode;
typedef enum tagintrMode { LOWI, CHANGE, RISING, FALLING } IntMode;


/*----------------------------------------------
 * ハードウェアの初期化処理
 *---------------------------------------------*/
void pinMode(PinName pin_name, PinMode mode);	// GPIOのIN/OUTを設定する


/*----------------------------------------------
 * 信号入出力
 *---------------------------------------------*/
void digitalWrite	(PinName pin_name, DigitalValue value);	// デジタル出力
int  digitalRead	(PinName pin_name);						// デジタル入力

void analogWrite	(PinName pin_name, int value);	// アナログ出力
int  analogRead		(PinName pin_name);				// アナログ入力


/*----------------------------------------------
 * タイマー処理
 *---------------------------------------------*/
// msミリ秒待つ
void delay(unsigned long ms);
// usマイクロ秒待つ
void delayMicroseconds(unsigned long us);


/*----------------------------------------------
 * シリアル通信
 *---------------------------------------------*/
void SerialBegin(char port, unsigned int baudrate);
char SerialAvailable(char port);
char SerialRead(char port);
void SerialPut(char port, char data);


#endif /* ARDUINO_H_ */
