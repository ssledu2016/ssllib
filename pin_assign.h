/*==================================================================================*
 *																					*
 * 「ARMチャレンジャー」I/Oマップと入出力モジュールの初期化　ヘッダーファイル							*
 * Revision: 1.0																	*
 * Author: SSL N.N.																	*
 * Date: 2016/02/02																	*
 *																					*
 *==================================================================================*/
#ifndef __PIN_ASSIGN_ARM_CHALLENGER_H__
#define __PIN_ASSIGN_ARM_CHALLENGER_H__


typedef enum tagPinName {
	PIN_PB5=0, PIN_PB0, PIN_PB1, PIN_PE4, PIN_PE5, PIN_PB4, PIN_PA5, PIN_PA6, PIN_PA7,

	PIN_PB2, PIN_PE0, PIN_PF0, PIN_PB7, PIN_PB6, PIN_PA4, PIN_PA3, PIN_PA2,

	PIN_PD0, PIN_PD1, PIN_PD2, PIN_PD3, PIN_PE1, PIN_PE2, PIN_PE3, PIN_PF1,

	PIN_PF2, PIN_PF3, PIN_PB3, PIN_PC4, PIN_PC5, PIN_PC6, PIN_PC7, PIN_PD6, PIN_PD7, PIN_PF4,

} PinName;

typedef enum tagPortName {
	PORT_A=0, PORT_B, PORT_C, PORT_D, PORT_E, PORT_F,
} PortName;


/*------------------------------------------------------------
 * ピン機能構造体　　TM4C123GH6PMのピン番号（シルク印刷）とI/Oポートの対応表
 *------------------------------------------------------------*/
typedef struct tagPinTable {
	// Pin assignment
	PinName 	pin;			// TM4C123GH6PMの外部入出力ピン番号（シルク印刷）
	PortName	peripheral_port;// ペリフェラルのポート番号
	char		peripheral_pin;	// ペリフェラルに対するピン番号

	// fucntion parameters
	char		adc;			// ADCポート番号
	struct tag_pwm {
		signed char  pin;		// PWM出力制御ロジックからの出力番号
		unsigned int param;		// PWM制御設定パラメータ
	} pwm[2];
	unsigned int timer_param;	// タイマー割り込みの設定パラメータ
} PinTable;

extern PinTable g_pintable[];	// ピン機能構造体の外部参照宣言

#define NA (0xFF)	/* ピン機能表の記載が”−”（機能が未割り当て）のときに使用する */


#endif // __PIN_ASSIGN_ARM_CHALLENGER_H__
