/*======================================================================================*
 * liblcd.c																				*
 *																						*
 * 「ARMチャレンジャー」LCD制御　ソースファイル														*
 * Revision: 1.0																		*
 * Author: SSL N.N.																		*
 * Date: 2016/02/02																		*
 *																						*
 *======================================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "inc/hw_ints.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/adc.h"
#include "driverlib/pin_map.h"
#include "driverlib/pwm.h"

#include "liblcd.h"
#include "arduino.h"


// LCDに使う端子を出力タイプに設定する
void pinModeLcd(void)
{
	pinMode(PIN_PA2, OUTPUT);
	pinMode(PIN_PA3, OUTPUT);
	pinMode(PIN_PA4, OUTPUT);
	pinMode(PIN_PA5, OUTPUT);
	pinMode(PIN_PE0, OUTPUT);
	pinMode(PIN_PE1, OUTPUT);
	return;
}

// RS信号の設定
void lcd_RS(unsigned char value)
{
	DigitalValue val = LOW;
	if (value != 0) val = HIGH;
	digitalWrite(PIN_PE1, val);
	return;
}

// 設定したコマンドを実行（E端子をL→H→L と切り替え）
void lcd_command()
{
	digitalWrite(PIN_PE0, HIGH);
	digitalWrite(PIN_PE0, LOW);
	return;
}

// データ転送（8ビットモード）
void lcd_transmit8(unsigned char data)
{
	unsigned char bit = data&0x10;
	// 上位4ビットを設定（下位4ビットの端子はオープン）
	digitalWrite(PIN_PA2, 0 == bit ? LOW : HIGH);
	bit = data&0x20;
	digitalWrite(PIN_PA3, 0 == bit ? LOW : HIGH);
	bit = data&0x40;
	digitalWrite(PIN_PA4, 0 == bit ? LOW : HIGH);
	bit = data&0x08;
	digitalWrite(PIN_PA5, 0 == bit ? LOW : HIGH);

	lcd_command();
	return;
}

// データ転送（4ビットモード）
void lcd_transmit4(unsigned char data)
{
	// 上位4ビットを設定
//	unsigned char temp = data >> 2;
//	GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5, temp);
	unsigned char bit = data&0x10;
	digitalWrite(PIN_PA2, 0 == bit ? LOW : HIGH);
	bit = data&0x20;
	digitalWrite(PIN_PA3, 0 == bit ? LOW : HIGH);
	bit = data&0x40;
	digitalWrite(PIN_PA4, 0 == bit ? LOW : HIGH);
	bit = data&0x80;
	digitalWrite(PIN_PA5, 0 == bit ? LOW : HIGH);
	lcd_command();

	// 下位4ビットを設定
//	temp = data << 2;
//	GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5, temp);
	bit = data&0x01;
	digitalWrite(PIN_PA2, 0 == bit ? LOW : HIGH);
	bit = data&0x02;
	digitalWrite(PIN_PA3, 0 == bit ? LOW : HIGH);
	bit = data&0x04;
	digitalWrite(PIN_PA4, 0 == bit ? LOW : HIGH);
	bit = data&0x08;
	digitalWrite(PIN_PA5, 0 == bit ? LOW : HIGH);
	lcd_command();

	return;
}

// LCDの初期化命令
void lcd_init(void)
{
	// RS信号をLowに設定
	lcd_RS(0);
	// 15 ミリ秒待ち
	delay(15);
	// 8ビットモードに設定（データビットに0011 **** = 0x30 を送る）
	lcd_transmit8(0x30);
	// 5 ミリ秒待ち
	delay(5);
	// 再度8ビットモードに設定（データビットに0011 **** = 0x30 を送る）
	lcd_transmit8(0x30);
	// 1 ミリ秒待ち
	delay(1);
	// 再々度8ビットモードに設定（データビットに0011 **** = 0x30 を送る）
	lcd_transmit8(0x30);
	// 5 ミリ秒待ち
	delay(5);
	// 4ビットモードに設定（データビットに0010 **** = 0x20 を送る）
	lcd_transmit8(0x20);
	// 1 ミリ秒待ち
	delay(1);
	// 初期設定（4ビット・バス、2行表示。データビットに0010 1000 = 0x28 を送る）
	lcd_transmit4(0x28);
	// 1 ミリ秒待ち
	delay(1);

	// 表示モードの設定
	// （文字表示ON、カーソルなし、ブリンクなし。データビットに0000 1100 = 0x0C を送る）
	lcd_transmit4(0x0C);
	// 1 ミリ秒待ち
	delay(1);
	// 入力モードの設定（アドレス+1、シフトなし。データビットに0000 0110 = 0x06 を送る）
	lcd_transmit4(0x06);
	// 1 ミリ秒待ち
	delay(1);
}

// LCDの表示をクリア
void lcd_clear(void) {
	// RS信号をLowに設定
	lcd_RS(0);
	// クリア命令
	lcd_transmit4(0x01);
	// 1 ミリ秒待ち
	delay(1);
}

// LCDに文字列を表示
void lcd_disp(char* str) {
	// RS信号をHighに設定
	lcd_RS(1);
	// 文字データ出力
	while (*str) {
		// 1バイト文字データを送信
		lcd_transmit4(*str++);
		// 1 ミリ秒待ち
		delay(1);
	}
}

// 1行目から2行目に改行
void lcd_NewLine(void) {
	// RS信号をLowに設定
	lcd_RS(0);
	// カーソルを2行目先頭に移動（0x80 + 0x40 = 0xC0）
	lcd_transmit4(0xC0);
	// 1 ミリ秒待ち
	delay(1);
}

// LCDのカーソルをホームポジションへ
void lcd_Home(void) {
	// RS信号をLowに設定
	lcd_RS(0);
	// カーソルをホームポジションに戻す
	lcd_transmit4(0x02);
	// 1 ミリ秒待ち
	delay(1);
}
