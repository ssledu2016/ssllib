/*
 * iomap_armc.h
 *
 *  Created on: 2016/02/24
 *      Author: kunsan
 */
/*======================================================================================*
 *																						*
 * 「ARMチャレンジャー」I/Oマップと入出力モジュールの初期化　ヘッダーファイル									*
 * Revision: 1.0																		*
 * Author: SSL N.N.																		*
 * Date: 2016/02/02																		*
 *																						*
 *======================================================================================*/
#ifndef __IOMAP_ARM_CHALLENGER_H__
#define __IOMAP_ARM_CHALLENGER_H__

#include "pin_assign.h"

typedef enum tagTimerMode { TIMEOUT, } TimerMode;



/*----------------------------------------------
 * I/Oマップ関連
 *---------------------------------------------*/
// ペリフェラルのアドレス取得
unsigned int getGpioPeriperal(PortName port);
unsigned int getAdcPeriperal(char module);
unsigned int getPwmPeriperal(char module);
unsigned int getTimerPeriperal(char module);

// ポート毎の入出力ベースアドレス取得
unsigned int getIoBase(PortName port);
unsigned int getAdcBase(char port);
unsigned int getPwmBase(char module);

// PWM制御関連
// PWM制御出力をするピン番号
unsigned int getPinAddress(int pin);
// PWM制御出力をするPWM制御ロジックのピン番号を取得する
unsigned int getPwmAddress(int pin);
// PWM制御をするピンの位置を取得する
unsigned int getPwmBit(int pin);

// 設定しているPWM制御周期を取得する
unsigned int getPwmPeriod(void);

// タイマーモジュール関連
unsigned int getTimerBase(int timer_module);
unsigned int getTimerType(int timer_module);
// タイマー割り込み番号を取得する
unsigned int getTimerIntNum(int timer_module);
// タイマー割り込みが発生する要因
unsigned int getTimerMode(TimerMode);

/*----------------------------------------------
 * I/Oモジュールの初期化
 *---------------------------------------------*/
// デジタル入力
void initDigitalInput	(PinName pin_name);
// デジタル出力
void initDigitalOutput	(PinName pin_name);
// アナログ入力
void initAnalogInput	(PinName pin_name);
// アナログ出力
void initAnalogOutput	(PinName pin_name);


/*----------------------------------------------
 * シリアル通信関連
 *---------------------------------------------*/
// UART初期化
void initUart(char port, unsigned int baudrate);
// UARTポートにデータがあるか
char UartAvarable(char port);
// UARTポートから1バイト読み取る
char UartRead(char port);
// UARTポートに１バイト書き込む
void UartPut(char port, char data);


#endif // __IOMAP_ARM_CHALLENGER_H__
