/*
 * test4-1.c
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */
#include "arduino.h"
#include "res_hw.h"


void test41_setup(void)
{
	pinMode(pin_vr[0], 	ADC);
	pinMode(pin_led[0], PWM);

	pinMode(pin_vr[1], 	ADC);
	pinMode(pin_led[1], PWM);
}

void test41_loop(void) {
	// アナログ入力の値を読み取る
	unsigned long ulData = analogRead(pin_vr[0]);
	// A/D変換の値からアナログ出力を変更する
	analogWrite(pin_led[0], ulData);

	// アナログ入力の値を読み取る
	unsigned long ulData2 = analogRead(pin_vr[1]);
	// A/D変換の値からアナログ出力を変更する
	analogWrite(pin_led[1], ulData2);
}
