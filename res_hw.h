/*
 * res_hw.h
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */

#ifndef RES_HW_H_
#define RES_HW_H_


extern PinName pin_led[];
extern PinName pin_psw[];
extern PinName pin_vr[];
typedef enum tagRgb {RED, GREEN, BLUE} RGB;
extern PinName pin_rgb_led[];
extern PinName pin_motor[];


#endif /* RES_HW_H_ */
