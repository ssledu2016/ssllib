/*
 * res_hw.c
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */
#include "driverlib/pin_map.h"
#include "arduino.h"

PinName pin_vr[]= {PIN_PE3, PIN_PE2};

PinName pin_led[] = {PIN_PD0,PIN_PD1,PIN_PD2,PIN_PD3,PIN_PC4,PIN_PC5,PIN_PC6,PIN_PC7};

PinName pin_psw[] = {PIN_PB2, PIN_PB3, PIN_PB4, PIN_PB5};

PinName pin_rgb_led[] = {PIN_PF1, PIN_PF3, PIN_PF2};

PinName pin_motor[] = {PIN_PE4, PIN_PE5};

