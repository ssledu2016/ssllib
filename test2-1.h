/*
 * test21.h
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */

#ifndef TEST2_1_H_
#define TEST2_1_H_


void test21_setup(void);
void test21_loop(void);


#endif /* TEST2_1_H_ */
