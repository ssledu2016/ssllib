/*
 * test1-1.h
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */

#ifndef TEST1_1_H_
#define TEST1_1_H_

void test11_setup(void);
void test11_loop(void);

#endif /* TEST1_1_H_ */
