/*
 * test21.c
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */
#include "arduino.h"
#include "res_hw.h"

void test21_setup(void)
{
	pinMode(pin_led[0],	OUTPUT);
	pinMode(pin_psw[0],	INPUT);
}

void test21_loop(void)
{
	int nSwStatus = digitalRead(pin_psw[0]);
	DigitalValue ledStatus = 0 == nSwStatus ? LOW : HIGH;

	digitalWrite(pin_led[0], ledStatus);
}
