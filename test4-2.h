/*
 * test4-2.h
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */

#ifndef TEST4_2_H_
#define TEST4_2_H_

void test42_setup(void);
void test42_loop(void);

#endif /* TEST4_2_H_ */
