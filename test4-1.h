/*
 * test41.h
 *
 *  Created on: 2016/04/18
 *      Author: ssl
 */

#ifndef TEST4_1_H_
#define TEST4_1_H_

void test41_setup(void);
void test41_loop(void);

#endif /* TEST4_1_H_ */
