/*======================================================================================*
 *																						*
 * 「ARMチャレンジャー」I/Oマップと入出力モジュールの初期化　ソースファイル								*
 * Revision: 1.0																		*
 * Author: SSL N.N.																		*
 * Date: 2016/04/18																		*
 *																						*
 *======================================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_ints.h"
// #include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
// #include "driverlib/interrupt.h"
#include "driverlib/adc.h"
#include "driverlib/pin_map.h"
#include "driverlib/pwm.h"
#include "driverlib/timer.h"

#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#include "iomap_armc.h"
#include "pin_assign.h"

/*----------------------------------------------
 * I/Oマップ関連
 *---------------------------------------------*/
// GPIOペリフェラルのアドレスを取得する
unsigned int getGpioPeriperal(PortName port)
{
	unsigned int peripherals[] = {
			SYSCTL_PERIPH_GPIOA, SYSCTL_PERIPH_GPIOB, SYSCTL_PERIPH_GPIOC,
			SYSCTL_PERIPH_GPIOD, SYSCTL_PERIPH_GPIOE, SYSCTL_PERIPH_GPIOF
	};

	return peripherals[port];
}

// ADCモジュールペリフェラルのアドレスを取得する
unsigned int getAdcPeriperal(char module)
{
	unsigned int peripheral = 0;

	if( '0' == module ) {
		peripheral = SYSCTL_PERIPH_ADC0;
	}
	else if( '1' == module ) {
		peripheral = SYSCTL_PERIPH_ADC1;
	}

	return peripheral;
}

// PWMモジュールペリフェラルのアドレスを取得する
unsigned int getPwmPeriperal(char module)
{
	unsigned int peripheral = 0;

	if( '0' == module ) {
		peripheral = SYSCTL_PERIPH_PWM0;
	}
	else if( '1' == module ) {
		peripheral = SYSCTL_PERIPH_PWM1;
	}

	return peripheral;
}
// タイマーモジュールペリフェラルのアドレスを取得する
unsigned int getTimerPeriperal(char module)
{
	unsigned int peripheral = 0;

	if( '0' == module ) {
		peripheral = SYSCTL_PERIPH_TIMER0;
	}
	else if( '1' == module ) {
		peripheral = SYSCTL_PERIPH_TIMER1;
	}

	return peripheral;
}


// タイマーモジュールのベースアドレスを取得する
unsigned int getTimerBase(int timer_module)
{
	if( timer_module < 0 || 5 < timer_module )	return 0;
	unsigned int iobases[] = {
			TIMER0_BASE, TIMER1_BASE, TIMER2_BASE,
			TIMER3_BASE, TIMER4_BASE, TIMER5_BASE
	};

	return iobases[timer_module];
}

unsigned int getTimerType(int timer_module)
{
  return TIMER_A;
}

unsigned int getTimerIntNum(int timer_module)
{
  return INT_TIMER0A;
}

unsigned int getTimerMode(TimerMode mode)
{
  return TIMER_TIMA_TIMEOUT;
}

// GPIOポートのベースアドレスを取得する
unsigned int getIoBase(PortName port)
{
	unsigned int bases[] = {
			GPIO_PORTA_BASE, GPIO_PORTB_BASE, GPIO_PORTC_BASE,
			GPIO_PORTD_BASE, GPIO_PORTE_BASE, GPIO_PORTF_BASE};

	return bases[port];
}

// ピン番号からGPIOピンアドレス（ビット）を取得する
unsigned int getPinAddress(int pin)
{
	unsigned int gpiopin = 0;

	if( 0 <= pin ) {
		gpiopin = GPIO_PIN_0 << pin;
	}

	return gpiopin;
}

// ADCのベースアドレスを取得する
unsigned int getAdcBase(char port)
{
	unsigned int iobase = 0;

	if('0' == port) {
		iobase = ADC0_BASE;
	}
	else if('1' == port) {
		iobase = ADC1_BASE;
	}

	return iobase;
}

// PWMのベースアドレスを取得する
unsigned int getPwmBase(char port)
{
	unsigned int iobase = 0;

	if('0' == port) {
		iobase = PWM0_BASE;
	}
	else if('1' == port) {
		iobase = PWM1_BASE;
	}

	return iobase;
}

// PWMピンのI/Oアドレスを取得する
unsigned int getPwmAddress(int pin)
{
	if(pin < 0 || 7 < pin)	return 0;

	unsigned int ioaddress[] = {
			PWM_OUT_0, PWM_OUT_1, PWM_OUT_2,
			PWM_OUT_3, PWM_OUT_4, PWM_OUT_5,
			PWM_OUT_6, PWM_OUT_7
	};

	return ioaddress[pin];
}

// PWMピンのビット位置を取得する
unsigned int getPwmBit(int pin)
{
	unsigned int pwmbit = 0;

	if( 0 <= pin ) {
		pwmbit = PWM_OUT_0_BIT << pin;
	}

	return pwmbit;
}

// PWMジェネレーターのアドレスを取得する
unsigned int getPwmGenerator(int pwmpin)
{
	if( pwmpin < 0 || 7 < pwmpin)	return 0;
	int iNum = pwmpin/2;
	//	0000 0100 0000
	//	0000 1000 0000
	//	0000 1100 0000
	//	0001 0000 0000
	unsigned int Generator[] = {PWM_GEN_0, PWM_GEN_1, PWM_GEN_2, PWM_GEN_3};

	return Generator[iNum];
}

// PWMモジュールに設定するパラメータを取得する
unsigned int getPwmParam(PinName pin_name, char pwm_module)
{
	if( '1' == pwm_module ) {
		return g_pintable[pin_name].pwm[1].param;
	}
	else {
		return g_pintable[pin_name].pwm[0].param;	// GPIO_PD0_M0PWM6
	}
}



/*----------------------------------------------
 * ハードウェアの初期化処理
 *---------------------------------------------*/

// デジタル入出ピンを入力タイプで初期化
void initDigitalInput (PinName pin_name)
{
	PortName port = g_pintable[pin_name].peripheral_port;
	int  pin	  = g_pintable[pin_name].peripheral_pin;

	unsigned int peripheral = getGpioPeriperal(port);
	unsigned int gpiobase   = getIoBase(port);
	unsigned int gpiopin    = getPinAddress(pin);

	// 使用するペリフェラルの有効化
	SysCtlPeripheralEnable(peripheral);
	// 入力タイプに設定
	GPIOPinTypeGPIOInput(gpiobase, gpiopin);

	return;
}

// デジタル入出ピンを出力タイプで初期化
void initDigitalOutput (PinName pin_name)
{
	PortName port = g_pintable[pin_name].peripheral_port;
	int  pin  	  = g_pintable[pin_name].peripheral_pin;

	unsigned int peripheral = getGpioPeriperal(port);
	unsigned int gpiobase   = getIoBase(port);
	unsigned int gpiopin    = getPinAddress(pin);

	// 使用するペリフェラルの有効化
	SysCtlPeripheralEnable(peripheral);
	// 出力タイプに設定
	GPIOPinTypeGPIOOutput(gpiobase, gpiopin);

	return;
}

// ADCのポートアドレスを取得する
unsigned int getAdcPortAddress(char channel)
{
	char address = ADC_CTL_CH0;
	if( channel <= 15) {
		address += channel;
	}

	return address;
}

PinName use_adc_ch[2] = {PIN_PE3, PIN_PE2};
int use_adc = 0;
// ADCの初期化
void initAnalogInput	(PinName pin_name)
{
	PortName port = g_pintable[pin_name].peripheral_port;
	int  pin  = g_pintable[pin_name].peripheral_pin;
	unsigned int adc = g_pintable[pin_name].adc;

	char adc_module = '0';				// ADCモジュールM0
	unsigned int adc_base_address = 0;
	if( 0 == use_adc ){
		use_adc_ch[0] = pin_name;
		adc_module = '0';
		adc_base_address = ADC0_BASE;
	}
	else if(1 == use_adc) {
		use_adc_ch[1] = pin_name;
		adc_module = '1';
		adc_base_address = ADC1_BASE;
	}
	unsigned int peripheral = getGpioPeriperal(port);
	unsigned int peripheraladc = getAdcPeriperal(adc_module);	// SYSCTL_PERIPH_ADC0
	unsigned int iobase     = getIoBase(port);					// GPIO_PORTE_BASE
	unsigned int gpiopin    = getPinAddress(pin);				// GPIO_PIN_3

	int adc_sequencer = 3;
	unsigned int adc_ch = getAdcPortAddress(adc);
	unsigned int adc_start_trigger = ADC_TRIGGER_PROCESSOR;

	// 使用するペリフェラルの有効化
	SysCtlPeripheralEnable(peripheral);
	SysCtlPeripheralEnable(peripheraladc);

	// A/D変換の設定
	// : PE3(AIN0)をADCに設定
	GPIOPinTypeADC(iobase, gpiopin);
	// : 使用するADCシーケンスの指定
	ADCSequenceConfigure(adc_base_address, adc_sequencer, adc_start_trigger, 0);
	// : ADCシーケンスの設定
	ADCSequenceStepConfigure(adc_base_address, adc_sequencer, 0, adc_ch | ADC_CTL_IE | ADC_CTL_END);
	// : ADCシーケンスを有効にする
	ADCSequenceEnable(adc_base_address, adc_sequencer);

	use_adc++;
	return;
}


unsigned int g_ulPeriod = 1;	// PWMの制御周期 [ms]
// PWMの制御周期を設定する
unsigned int getPwmPeriod(void)
{
	return g_ulPeriod;
}

// PWMの初期化
void initAnalogOutput	(PinName pin_name)
{
	PortName port = g_pintable[pin_name].peripheral_port;
	int  pin  = g_pintable[pin_name].peripheral_pin;
	signed char  pwmpin  = g_pintable[pin_name].pwm[0].pin;
	char pwm_module = '0';	// PWMモジュールM0
	if((signed char)NA == pwmpin) {
		pwmpin = g_pintable[pin_name].pwm[1].pin;
		pwm_module++;
	}


	unsigned long ulPeripheral = getGpioPeriperal(port);			// SYSCTL_PERIPH_GPIOD
	unsigned long ulPeripheralpwm = getPwmPeriperal(pwm_module);	// SYSCTL_PERIPH_PWM0
	unsigned long ulIobase     = getIoBase(port);					// GPIO_PORTD_BASE
	unsigned long ulGpiopin    = getPinAddress(pin);				// GPIO_PIN_0
	unsigned long ulPwmbase    = getPwmBase(pwm_module);			// PWM0_BASE
	unsigned long ulPwmAddress = getPwmAddress(pwmpin);				// PWM_OUT_6
	unsigned long ulPwmbit     = getPwmBit(pwmpin);					// PWM_OUT_6_BIT
	// PWMモジュールはM0を、出力チャンネルはPWM6を指定する。
	unsigned long ulPwm_module_to_ch = getPwmParam(pin_name, pwm_module);
	// PWMジェネレーター2を選択
	unsigned long ulPwmGenerator = getPwmGenerator(pwmpin);


  // 使用するペリフェラルの有効化
	SysCtlPeripheralEnable(ulPeripheral);
	SysCtlPeripheralEnable(ulPeripheralpwm);

  // PWMの設定
	// : PD0(M0PWM6)をPWMに設定
	GPIOPinTypePWM(ulIobase, ulGpiopin);
	// : PD0をM0PWM6に設定
	GPIOPinConfigure(ulPwm_module_to_ch);
	// : PWMの周期を指定　周期： 1ms
	g_ulPeriod = SysCtlClockGet() / 1000;
	// : PWMジェネレータの設定
	PWMGenConfigure(ulPwmbase, ulPwmGenerator, PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);
	// : PWMジェネレータの周期を設定
	PWMGenPeriodSet(ulPwmbase, ulPwmGenerator, g_ulPeriod);
	// : PWM信号のデューティー比を９０％に設定
	PWMPulseWidthSet(ulPwmbase, ulPwmAddress, g_ulPeriod * 9 / 10);
	// : PWM0の出力を許可
	PWMOutputState(ulPwmbase, ulPwmbit, true);
	// : PWM開始（PWMジェネレータの有効化）
	PWMGenEnable(ulPwmbase, ulPwmGenerator);

	return;
}


// シリアルポートにボーレートを設定して初期化する
void initUart(char port, unsigned int baudrate)
{
	// 使用するペリフェラルの有効化
	// UART0 初期化用
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	// ポートA（シリアル通信 用）
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // GPIOピンPA0,PA1をシリアル通信のRX,TXに割り当てる
    GPIOPinConfigure(GPIO_PA0_U0RX);	// 受信 RX
    GPIOPinConfigure(GPIO_PA1_U0TX);	// 送信 TX
    // GPIOピンをUART通信用に設定する
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    if( 0 == port ) {
        // UARTポートのクロックを内部オシレーター（16MHz）から取る
        UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

        // PCとのシリアル通信のためにUART0ポートの通信速度を設定する
        UARTStdioConfig(0, baudrate, SysCtlClockGet());
    }
    else if( 1 == port ) {
        //通信速度、通信仕様の設定
        UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), baudrate,
                (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
    }

    return;
}

char UartAvarable(char port)
{
	return UARTCharsAvail(UART0_BASE);
}

char UartRead(char port)
{
	return UARTCharGet(UART0_BASE);
}

void UartPut(char port, char data)
{
	UARTCharPut(UART0_BASE, data);
	return;
}
