/*======================================================================================*
 *																						*
 * 「ARMチャレンジャー」Arduino互換ライブラリ化　ソースファイル								*
 * Revision: 1.0																		*
 * Author: SSL N.N.																		*
 * Date: 2016/04/18																		*
 *																						*
 *======================================================================================*/
#include <stdint.h>
#include <stdbool.h>

#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/adc.h"

#include "driverlib/pwm.h"
#include "driverlib/timer.h"

#include "utils/uartstdio.h"
#include "utils/ustdlib.h"

#include "iomap_armc.h"
#include "arduino.h"


/*----------------------------------------------
 * ハードウェアの初期化処理
 *---------------------------------------------*/

// GPIOのIN/OUTを設定する
void pinMode(PinName pin_name, PinMode mode)
{
	switch( mode ) {
	case INPUT :
		initDigitalInput(pin_name);
		break;
	case OUTPUT :
		initDigitalOutput(pin_name);
		break;
	case ADC :
		initAnalogInput(pin_name);
		break;
	case PWM :
		initAnalogOutput(pin_name);
		break;
	case INPUT_PULLUP :
		break;
	default:
		break;
	}

	return;
}


/*----------------------------------------------
 * 信号入出力
 *---------------------------------------------*/

// 任意の端子へデジタル信号を出力する
void digitalWrite	(PinName pin_name, DigitalValue value)
{
	PortName port = g_pintable[pin_name].peripheral_port;
	int  pin  = g_pintable[pin_name].peripheral_pin;

	unsigned int  gpiobase = getIoBase(port);
	unsigned char gpiopin  = getPinAddress(pin);
	unsigned char output   = HIGH == value ? gpiopin : 0x00;

	GPIOPinWrite(gpiobase, gpiopin, output);

	return;
}

// 任意の端子へ入力されたデジタル信号を読み取る
int digitalRead(PinName pin_name)
{
	PortName port = g_pintable[pin_name].peripheral_port;
	int  	 pin  = g_pintable[pin_name].peripheral_pin;

	unsigned int value = GPIOPinRead(getIoBase(port), getPinAddress(pin));	// 端子からデジタル値を読み取る
	unsigned int address = getPinAddress(pin);// ビット位置を取得する
	unsigned int status = value & address;	// 該当ビットの情報だけ取り出す（マスク処理）
	value = status == 0 ? 0 : 1;

	return value;
}

// 任意の端子へアナログ量をPWM制御で信号出力する
void analogWrite	(PinName pin_name, int value)
{
	char pwm_module = 0;
	int  pwmpin  = g_pintable[pin_name].pwm[pwm_module].pin;
	if((signed char)NA == pwmpin) {
		pwmpin  = g_pintable[pin_name].pwm[++pwm_module].pin;
	}

	unsigned long ulIoBase    = getPwmBase('0'+pwm_module);
	unsigned long ulIoAddress = getPwmAddress(pwmpin);
	unsigned long ulOutput    = getPwmPeriod() * value / 4097;

	// A/D変換の値に応じてデューティ比を変更
	PWMPulseWidthSet(ulIoBase, ulIoAddress, ulOutput);

	return;
}

extern PinName use_adc_ch[2];
// 任意の端子に入力されたアナログ信号のA/D変換値を読み取る
int analogRead (PinName pin_name)
{
	// A/D変換値格納用
	unsigned int data = 0;
	char adc_module = '0';
	if(pin_name == use_adc_ch[0]) {
		adc_module= '0';
	}
	else if(pin_name == use_adc_ch[1]) {
		adc_module= '1';
	}
	unsigned int base = getAdcBase(adc_module);
	int adc_sequencer = 3;

	// A/D変換開始
	ADCProcessorTrigger(base, adc_sequencer);
	// A/D変換値の取得
	ADCSequenceDataGet(base, adc_sequencer, &data);

	return data;
}


/*----------------------------------------------
 * 割り込み処理
 *---------------------------------------------*/
volatile int is_init_timer = 0;
volatile long g_WaitCount = 0;	// delay関数用
#define TIME_RESOLUTION 500000

// タイマー割り込みハンドラ｜カウンター
void SysTickHandler(void)
{
	// g_cntをデクリメント
	g_WaitCount -= 1000000/TIME_RESOLUTION;
	return;
}

// タイマー割り込みの初期化
void initTick()
{
	// SysTickの割り込みが発生する間隔を設定
	SysTickPeriodSet(SysCtlClockGet() / TIME_RESOLUTION);
	// 割り込み時に呼び出す処理（関数）を指定
	SysTickIntRegister(SysTickHandler);
	return;
}

// タイマー周期の変更
void setTimerPeriod (int timer_module, unsigned int hz)
{
	if (hz < 1) hz = 1;	// ゼロ割り算防止

	unsigned int iobase = getTimerBase(timer_module);
	unsigned int type   = getTimerType(timer_module);

	// 周期を0.1?1秒に変更する
	TimerLoadSet(iobase, type, SysCtlClockGet() / hz);

	return;
}


/*----------------------------------------------
 * タイマー処理（時間計測処理）
 *---------------------------------------------*/
// usマイクロ秒待つ
void delayMicroseconds(unsigned long us)
{
	// SysCtlDelay(SysCtlClockGet()/3);
	if(0 == is_init_timer) {
		initTick();
		is_init_timer = 1;
	}
	// カウント（時間）セット
	g_WaitCount = us;
	// SysTickを有効（カウント開始）
	SysTickEnable();
	// g_WaitCountが0 より小さくなるまでループ（割り込みルーチンでg_cntをデクリメント）
	while (0 < g_WaitCount)	/* empty */;
	// SysTickを停止（カウント停止）
	SysTickDisable();

	return;
}

// msミリ秒待つ
void delay(unsigned long ms)
{
	if(0 == is_init_timer) {
		initTick();
		is_init_timer = 1;
	}
	// カウント（時間）セット
	g_WaitCount = ms*1000;
	// SysTickを有効（カウント開始）
	SysTickEnable();
	// g_WaitCountが0 より小さくなるまでループ（割り込みルーチンでg_cntをデクリメント）
	while (0 < g_WaitCount)	/* empty */;
	// SysTickを停止（カウント停止）
	SysTickDisable();

	return;
}


/*----------------------------------------------
 * シリアル通信
 *---------------------------------------------*/
// 受信バッファのサイズ
#define MAX_RECV_LEN 255
// コマンドを受信するバッファ
char g_recvbuff[MAX_RECV_LEN];	// UARTシリアルポート用の受信バッファ
char *pbuff_head = g_recvbuff;	// UARTシリアルポートからのデータを取り出すポインタ

// 任意のシリアル通信ポートを初期化する
void SerialBegin(char port, unsigned int baudrate)
{
	initUart(port, baudrate);
    return;
}

// 指定したシリアルポートに受信データがあるか確認する
char SerialAvailable(char port)
{
	if(0 == port) {
		return (UartAvarable(port) || 0 != *pbuff_head);
	}
	else if(1 == port) {
		return UartAvarable(port);
	}
	return (0);
}

// 指定したシリアルポートからデータを１バイト読み取る
char SerialRead(char port)
{
	if( 0 == port ) {
		char ret = 0;

		if( g_recvbuff == pbuff_head ) {
			// 配列の先頭だったらシリアルポートからデータ取り出し保存する
		    UARTgets(g_recvbuff, sizeof(g_recvbuff));
		    pbuff_head = g_recvbuff;
		}

		// データを保存する
		ret = *pbuff_head;

		if(0 != *pbuff_head) {
			// NULL文字でなければ、次の文字を取り出すためにポインタを進める。
		    pbuff_head++;	// 次の要素へ
		    if( pbuff_head == g_recvbuff+MAX_RECV_LEN ) {
		    	// 配列の終端を超えていたら、ポインタを配列の先頭に戻す。
		    	pbuff_head = g_recvbuff;
		    }
		}
		else {
			// NULL文字だったらポインタを配列の先頭に戻す。
		    pbuff_head = g_recvbuff;
		}
	    return (ret);
	}
	else if(1 == port) {
		return UartRead(port);
	}

	return (0);
}

// 指定したシリアルポートにデータを１バイト書き込む
void SerialPut(char port, char data)
{
	if(0 == port) {
		char buff[2] = {0};
		buff[0] = data;
		UARTprintf(buff);
	}
	else if( 1 == port ) {
		UartPut(port, data);
	}

	return;
}
