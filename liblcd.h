/*
 * liblcd.h
 *
 *  Created on: 2016/02/26
 *      Author: kunsan
 */
/*======================================================================================*
 *																						*
 * 「ARMチャレンジャー」LCD制御　ヘッダーファイル								*
 * Revision: 1.0																		*
 * Author: SSL N.N.																		*
 * Date: 2016/02/02																		*
 *																						*
 *======================================================================================*/

#ifndef LIB_LIBLCD_H_
#define LIB_LIBLCD_H_


void pinModeLcd(void);
/*----------------------------------------------
 * LCD制御
 *---------------------------------------------*/
void lcd_RS(unsigned char value);	// RS信号の設定
void lcd_command();// 設定したコマンドを実行（E端子をL→H→L と切り替え）
void lcd_transmit8(unsigned char data);	// 上位4ビットを設定（下位4ビットの端子はオープン）
void lcd_transmit4(unsigned char data);	// データ転送（4ビットモード）
void lcd_init(void);	// LCDの初期化
void lcd_clear(void);	// LCDの表示をクリア
void lcd_disp(char* s);	// LCDに文字列を表示
void lcd_NewLine(void);	// 1行目から2行目に改行
void lcd_Home(void);	// LCDのカーソルをホームポジションへ


#endif /* LIB_LIBLCD_H_ */
