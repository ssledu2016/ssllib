/*
 * pin_assign.c
 *
 *  Created on: 2016/02/24
 *      Author: kunsan
 */
#include "pin_assign.h"

#include "driverlib/pin_map.h"


PinTable g_pintable[] = {
	{ PIN_PB5, PORT_B, 5, 11, {{3,GPIO_PB5_M0PWM3}, {0,0}}, GPIO_PB5_T1CCP1 },
	{ PIN_PB0, PORT_B, 0, NA, {{NA,0}, {NA,0}}, GPIO_PB0_T2CCP0 },
	{ PIN_PB1, PORT_B, 1, NA, {{NA,0}, {NA,0}}, GPIO_PB1_T2CCP1 },
	{ PIN_PE4, PORT_E, 4, 9,  {{4,GPIO_PE4_M0PWM4}, {2,GPIO_PE4_M1PWM2}}, 0 },
	{ PIN_PE5, PORT_E, 5, 8,  {{5,GPIO_PE5_M0PWM5}, {3,GPIO_PE5_M1PWM3}}, 0 },
	{ PIN_PB4, PORT_B, 4, 10, {{2,GPIO_PB4_M0PWM2}, {0,0}}, GPIO_PB4_T1CCP0 },
	{ PIN_PA5, PORT_A, 5, NA, {{NA,0}, {NA,0}}, 0 },
	{ PIN_PA6, PORT_A, 6, NA, {{NA,0}, {2,GPIO_PA6_M1PWM2}}, 0 },
	{ PIN_PA7, PORT_A, 7, NA, {{NA,0}, {3,GPIO_PA7_M1PWM3}}, 0 },		// ピン機能表が間違っている

	{ PIN_PB2, PORT_B, 2, NA, {{NA,0}, {NA,0}}, GPIO_PB2_T3CCP0 },
	{ PIN_PE0, PORT_E, 0, NA, {{NA,0}, {NA,0}}, 0 },
	{ PIN_PF0, PORT_F, 0, NA, {{NA,0}, {4,GPIO_PF0_M1PWM4}}, GPIO_PF0_T0CCP0 },
	{ PIN_PB7, PORT_B, 7, NA, {{1,GPIO_PB7_M0PWM1}, {NA,0}}, GPIO_PB7_T0CCP1 },
	{ PIN_PB6, PORT_B, 6, NA, {{0,GPIO_PB6_M0PWM0}, {NA,0}}, GPIO_PB6_T0CCP0 },
	{ PIN_PA4, PORT_A, 4, NA, {{NA,0}, {NA,0}}, 0 },
	{ PIN_PA3, PORT_A, 3, NA, {{NA,0}, {NA,0}}, 0 },
	{ PIN_PA2, PORT_A, 2, NA, {{NA,0}, {NA,0}}, 0 },

	{ PIN_PD0, PORT_D, 0, 7,  {{6,GPIO_PD0_M0PWM6}, {0,GPIO_PD0_M1PWM0}}, GPIO_PD0_WT2CCP0 },
	{ PIN_PD1, PORT_D, 1, 6,  {{7,GPIO_PD1_M0PWM7}, {1,GPIO_PD1_M1PWM1}}, GPIO_PD1_WT2CCP1 },
	{ PIN_PD2, PORT_D, 2, 5,  {{NA,0}, {NA,0}}, GPIO_PD2_WT3CCP0 },
	{ PIN_PD3, PORT_D, 3, 4,  {{NA,0}, {NA,0}}, GPIO_PD3_WT3CCP1 },	// ピン機能表が間違っている
	{ PIN_PE1, PORT_E, 1, 2,  {{NA,0}, {NA,0}}, 0 },
	{ PIN_PE2, PORT_E, 2, 1,  {{NA,0}, {NA,0}}, 0 },
	{ PIN_PE3, PORT_E, 3, 0,  {{NA,0}, {NA,0}}, 0 },
	{ PIN_PF1, PORT_F, 1, NA, {{NA,0}, {5,GPIO_PF1_M1PWM5}}, GPIO_PF1_T0CCP1 },

	{ PIN_PF2, PORT_F, 2, NA, {{NA,0}, {6,GPIO_PF2_M1PWM6}}, GPIO_PF2_T1CCP0 },
	{ PIN_PF3, PORT_F, 3, NA, {{NA,0}, {7,GPIO_PF3_M1PWM7}}, GPIO_PF3_T1CCP1 },
	{ PIN_PB3, PORT_B, 3, NA, {{NA,0}, {NA,0}}, GPIO_PB3_T3CCP1 },
	{ PIN_PC4, PORT_C, 4, NA, {{6,GPIO_PC4_M0PWM6}, {NA,0}}, GPIO_PC4_WT0CCP0 },
	{ PIN_PC5, PORT_C, 5, NA, {{7,GPIO_PC5_M0PWM7}, {NA,0}}, GPIO_PC5_WT0CCP1 },
	{ PIN_PC6, PORT_C, 6, NA, {{NA,0}, {NA,0}}, GPIO_PC6_WT1CCP0 },
	{ PIN_PC7, PORT_C, 7, NA, {{NA,0}, {NA,0}}, GPIO_PC7_WT1CCP1 },
	{ PIN_PD6, PORT_D, 6, NA, {{NA,0}, {NA,0}}, GPIO_PD6_WT5CCP0 },
	{ PIN_PD7, PORT_D, 7, NA, {{NA,0}, {NA,0}}, GPIO_PD7_WT5CCP1 },
	{ PIN_PF4, PORT_F, 4, NA, {{NA,0}, {NA,0}}, GPIO_PF4_T2CCP0 },
};
