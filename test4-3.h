/*
 * test4-3.h
 *
 *  Created on: 2016/04/21
 *      Author: ssl
 */

#ifndef TEST4_3_H_
#define TEST4_3_H_

void test43_setup(void);
void test43_loop(void);

#endif /* TEST4_3_H_ */
