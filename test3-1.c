/*
 * test3-1.c
 *
 *  Created on: 2016/04/26
 *      Author: ssl
 */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "driverlib/sysctl.h"


#include "iomap_armc.h"
#include "arduino.h"
#include "res_hw.h"

#include "liblcd.h"


void test31_setup()
{
	// 使用するペリフェラルの有効化
	// スイッチの端子を入力タイプに設定
	pinMode(pin_psw[0], INPUT);
	pinMode(pin_psw[1], INPUT);
	pinMode(pin_psw[2], INPUT);
	pinMode(pin_psw[3], INPUT);

	// VR1の端子をADCに設定
	pinMode(pin_vr[0], ADC);

	// カラーLEDの端子をPWMに設定
	pinMode(pin_rgb_led[0], PWM);
	pinMode(pin_rgb_led[1], PWM);
	pinMode(pin_rgb_led[2], PWM);

	// LCDで使う端子をタイプを設定する
	pinModeLcd();

	// LCD初期化
	lcd_init();
	// LCD表示内容クリア
	lcd_clear();

	return;
}

// 10進数から16進数への文字の変換
char dec2hexstr(unsigned long data)
{
	char ret = '0' + data;
	if ('9' < ret ) {
		ret = ret + ('A' - '9' -1);
	}
	return ret;
}


#define MAX_ADC_COUNT 4096

#define ALPHA_NUM 0x07
#define MAX_RGB_NUM 256

// LED用
unsigned long color[3] = {0};	// RGBカラーコードの設定
int g_mode_rgb = 3;	// RGB設定モード

// LCDへの出力文字列
// 表示文字列一行目
char szText1[17] = {"***ﾌﾙｶﾗｰﾋｮｳｼﾞ***"};
// 表示文字列二行目
char szText2[17] = {"****************"};

void test31_loop()
{
	// A/D変換値の取得
	uint32_t uiData = analogRead(pin_vr[0]);

	// A/D変換の値に応じてデューティ比を決定する
	if( uiData < 20 ) {
		// ボリュームを最小に絞っても０にならないので、
		// 下限値をもうけて０を設定できるようにする
		uiData = 0;
	}
	if (0 <= g_mode_rgb && g_mode_rgb <= 2) {
		color[g_mode_rgb] = uiData;
	}

	// LEDへのPWM出力を設定
	analogWrite(pin_rgb_led[0], color[0]);
	analogWrite(pin_rgb_led[1], color[2]);
	analogWrite(pin_rgb_led[2], color[1]);

	// スイッチ読み取り
	if (0 == digitalRead(pin_psw[0])) {
		g_mode_rgb = 0;
	}
	if (0 == digitalRead(pin_psw[1])) {
		g_mode_rgb = 1;
	}
	if (0 == digitalRead(pin_psw[2])) {
		g_mode_rgb = 2;
	}
	if (0 == digitalRead(pin_psw[3])) {
		g_mode_rgb = 3;
	}

	// 数値変換用（文字列の最後が常に\0で設定するように注意する）
	char temp[2] = {0};
	// 2行目
	strcpy(szText2, "#");
	// REDの2桁目
	temp[0] = dec2hexstr(MAX_RGB_NUM * color[0] / MAX_ADC_COUNT / 16);
	strcat(szText2, temp);
	// REDの1桁目
	temp[0] = dec2hexstr(MAX_RGB_NUM * color[0] / MAX_ADC_COUNT % 16);
	strcat(szText2, temp);

	// GREENの2桁目
	temp[0] = dec2hexstr(MAX_RGB_NUM * color[1] / MAX_ADC_COUNT / 16);
	strcat(szText2, temp);
	// GREENの1桁目
	temp[0] = dec2hexstr(MAX_RGB_NUM * color[1] / MAX_ADC_COUNT % 16);
	strcat(szText2, temp);

	// BLUEの2桁目
	temp[0] = dec2hexstr(MAX_RGB_NUM * color[2] / MAX_ADC_COUNT / 16);
	strcat(szText2, temp);
	// BLUEの1桁目
	temp[0] = dec2hexstr(MAX_RGB_NUM * color[2] / MAX_ADC_COUNT % 16);
	strcat(szText2, temp);
	strcat(szText2, "---------");

	// LCDに1行目を表示
	lcd_disp(szText1);
	lcd_NewLine();	// 改行

	// LCDに2行目を表示
	lcd_disp(szText2);
	lcd_Home();// カーソルをホームへ

	return;
}


