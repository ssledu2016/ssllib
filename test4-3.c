/*
 * test4-3.c
 *
 *  Created on: 2016/04/21
 *      Author: ssl
 */
#include "arduino.h"
#include "res_hw.h"

void test43_setup(void)
{
	pinMode(pin_psw[0], INPUT);
	pinMode(pin_psw[1], INPUT);
	pinMode(pin_psw[2], INPUT);

	pinMode(pin_rgb_led[RED], 	PWM);
	pinMode(pin_rgb_led[GREEN], PWM);
	pinMode(pin_rgb_led[BLUE], 	PWM);

	pinMode(pin_vr[0], ADC);
}

void test43_loop(void)
{
	// アナログ入力の値を読み取る
	unsigned long ulData = analogRead(pin_vr[0]);

	// A/D変換の値からアナログ出力を変更する
	if( 0 == digitalRead(pin_psw[0]) ) {
		analogWrite(pin_rgb_led[RED], ulData);
	}
	else if( 0 == digitalRead(pin_psw[1]) ) {
		analogWrite(pin_rgb_led[GREEN], ulData);
	}
	else if( 0 == digitalRead(pin_psw[2]) ) {
		analogWrite(pin_rgb_led[BLUE], ulData);
	}
}


